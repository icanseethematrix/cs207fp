#pragma once
/** @file Mesh.hpp
 * @brief A Mesh is composed of nodes, edges, and triangles such that:
 *  -- All triangles have three nodes and three edges.
 *  -- All edges belong to at least one triangle and at most two triangles.
 */

 #include "Graph.hpp"
 #include "Point.hpp"
 #include <vector>

/** @class Mesh
 * @brief A template for 3D triangular meshes.
 *
 * Users can add triangles and retrieve nodes, edges, and triangles.
 */
template <typename N, typename E, typename T>
class Mesh {
  // Write all typedefs, public member functions, private member functions,
  //   inner classes, private member data, etc.

 public:

  Mesh() {}

  ~Mesh() = default;

  /** Type of indexes and sizes. Return type of Mesh::num_nodes(). */
  typedef int size_type;

  typedef T triangle_value_type;

  class Triangle;

  struct edgeTriangles: E {
    size_type tri1_uid;
    size_type tri2_uid;
    E edgeData;
  };

  typedef typename Graph<N, edgeTriangles>::node_type Node;
  typedef typename Graph<N, edgeTriangles>::node_type node_type;

  typedef typename Graph<N, edgeTriangles>::node_iterator node_iterator;
  typedef typename Graph<N, edgeTriangles>::edge_iterator edge_iterator;

  /** Return the number of triangles in the mesh. */
  size_type num_triangles() const {
    return triangles_.size();
  }

  /** Return the number of nodes in the mesh. */
  size_type num_nodes() const {
    return graph_.num_nodes();
  }

  /** Return the number of edges in the mesh. */
  size_type num_edges() const {
    return graph_.num_edges();
  }

  Node add_node(Point x) {
    nodeAdjTriangles.resize(nodeAdjTriangles.size()+1);
    return graph_.add_node(x);
  }

private:

  struct internal_triangle{
    size_type node1_uid;
    size_type node2_uid;
    size_type node3_uid;
    triangle_value_type value;
  };

  std::vector<std::vector<size_type>> nodeAdjTriangles;

  std::vector<internal_triangle> triangles_;

  Graph<N, edgeTriangles> graph_;



public:
  class Triangle {

    public:
      triangle_value_type& value(){
        return mesh_->triangles_[tri_uid_].value;
      }

      size_type index() {
        return tri_uid_;
      }

      bool is_valid() {
        return tri_uid_ != -1;
      }
      
      const triangle_value_type value() const {
        return mesh_->triangles_[tri_uid_].value;
      }

      Node node1() {
        return mesh_->graph_.node(mesh_->triangles_[tri_uid_].node1_uid);
      }
      Node node2() {
        return mesh_->graph_.node(mesh_->triangles_[tri_uid_].node2_uid); 
      }
      Node node3() {
        return mesh_->graph_.node(mesh_->triangles_[tri_uid_].node3_uid);
      }

      
      Triangle adjTriangle(Node x, Node y) const {
        auto edge = mesh_->graph_.edge(x.uid(), y.uid());
        if (edge.value().tri1_uid == tri_uid_) {
          auto tri2uid = edge.value().tri2_uid;
          return mesh_->triangle(tri2uid);
        }
        return mesh_->triangle(edge.value().tri1_uid);
      }


      float area() {
        return std::abs(node1().position().x*(node2().position().y-node3().position().y) + 
                          node2().position().x*(node3().position().y-node1().position().y) + 
                          node3().position().x*(node1().position().y-node2().position().y))/2.0;
      }

      Point center() {
        double a = (node1().position().x + node2().position().x + node3().position().x)/3;
        double b = (node1().position().y + node2().position().y + node3().position().y)/3;
        return Point(a, b, 0);
      }


      Point normal(Node x, Node y) {
        Node opp;
        if ((int(x.index()) == mesh_->triangles_[tri_uid_].node2_uid  && 
             int(y.index()) == mesh_->triangles_[tri_uid_].node1_uid) || (int(x.index()) == mesh_->triangles_[tri_uid_].node1_uid  && 
             int(y.index()) == mesh_->triangles_[tri_uid_].node2_uid))
            opp = mesh_->graph_.node(mesh_->triangles_[tri_uid_].node3_uid);

        else if ((int(x.index()) == mesh_->triangles_[tri_uid_].node3_uid  && 
                int(y.index()) == mesh_->triangles_[tri_uid_].node2_uid) || (int(x.index()) == mesh_->triangles_[tri_uid_].node2_uid  && 
                int(y.index()) == mesh_->triangles_[tri_uid_].node3_uid))
          opp = mesh_->graph_.node(mesh_->triangles_[tri_uid_].node1_uid);

        else
          opp = mesh_->graph_.node(mesh_->triangles_[tri_uid_].node2_uid);

        Point p1 = x.position() - y.position();
        Point p2 = x.position() - opp.position();
        Point p3 = Point(p1.y, -p1.x, 0);
        if (dot(p3, p2) < 0)
          return Point(-p3.x, -p3.y, 0);
        return p3;      
      }

    private:
      friend class Mesh;

      Mesh* mesh_;
      size_type tri_uid_;

      Triangle(Mesh* mesh, size_type uid):mesh_(const_cast<Mesh*>(mesh)), tri_uid_(uid){ 
      }

    };

  Triangle triangle(size_type i) {
    return Triangle(this, i);
  }

  class triangle_iterator {
    public:
      triangle_iterator() {

      }

      Triangle operator*() const {
        return parent_mesh_->triangle(iterator_index_);
      }

      triangle_iterator& operator++() {
        ++iterator_index_;
        return *this;
      }

      bool operator!=(const triangle_iterator& iterator2) const {
        return iterator_index_ != iterator2.iterator_index_;
      }

      bool operator==(const triangle_iterator& iterator2) const {
        return parent_mesh_ == iterator2.parent_mesh_ && iterator_index_ == iterator2.iterator_index_;
      }

    private:
      friend class Mesh;
      Mesh* parent_mesh_;
      size_type iterator_index_;

      triangle_iterator(const Mesh* mesh, size_type index): parent_mesh_(const_cast<Mesh*>(mesh)), iterator_index_(index){}

  };

  triangle_iterator triangle_begin() {
    return triangle_iterator(this, 0);
  }

  triangle_iterator triangle_end() {
    return triangle_iterator(this, triangles_.size());
  }

  class node_incident_iterator {
    public:
      int getIndex() {
        return idx;
      }

      node_incident_iterator(){}

      Triangle operator*() const {
        return parent_mesh->triangle(parent_mesh->nodeAdjTriangles[nodeId][idx]);
      }

      node_incident_iterator& operator++() {
        ++idx;
        return *this;
      }

      bool operator==(const node_incident_iterator& iterator2) const {
        return ((parent_mesh->graph_.node(nodeId) == iterator2.parent_mesh->graph_.node(nodeId)) && (idx == iterator2.idx));
      }

      bool operator!=(const node_incident_iterator& iterator2) const {
        return !((parent_mesh->graph_.node(nodeId) == iterator2.parent_mesh->graph_.node(nodeId)) && (idx == iterator2.idx));         
      }

    private:
      friend class Mesh;
      Mesh* parent_mesh;
      size_type idx;
      size_type nodeId;

    node_incident_iterator(Mesh* m, size_type it_index, size_type uid): parent_mesh(const_cast<Mesh*>(m)), idx(it_index), nodeId(uid){}

  };

  node_incident_iterator adj_triangle_begin(size_type uid){
    return node_incident_iterator(this, 0, uid);
  }

  node_incident_iterator adj_triangle_end(size_type uid) {
    return node_incident_iterator(this, nodeAdjTriangles[uid].size(), uid);
  }

  Triangle add_triangle(const Node& x, const Node& y, const Node& z) {
    edgeTriangles newTriangleEdge;
    newTriangleEdge.tri1_uid = triangles_.size();
    newTriangleEdge.tri2_uid = -1; //make invalid triangle

    if (graph_.has_edge(x, y)){
      graph_.edge(x.uid(), y.uid()).value().tri2_uid = triangles_.size();
    }else{
      graph_.add_edge(x, y, newTriangleEdge);
    }

    if (graph_.has_edge(y, z)){
      graph_.edge(y.uid(), z.uid()).value().tri2_uid = triangles_.size();
    }else{
      graph_.add_edge(y, z, newTriangleEdge);
    }

    if (graph_.has_edge(z, x)){
      graph_.edge(z.uid(), x.uid()).value().tri2_uid = triangles_.size();
    }else{
      graph_.add_edge(z, x, newTriangleEdge);
    }    

    nodeAdjTriangles[x.uid()].push_back(triangles_.size());
    nodeAdjTriangles[y.uid()].push_back(triangles_.size());
    nodeAdjTriangles[z.uid()].push_back(triangles_.size());

    internal_triangle newTraingle;
    newTraingle.node1_uid = x.uid();
    newTraingle.node2_uid = y.uid();
    newTraingle.node3_uid = z.uid();

    newTraingle.value = triangle_value_type();
    triangles_.push_back(newTraingle);
    return triangle(triangles_.size() - 1);
  }

  node_iterator node_begin() {
    return graph_.node_begin();
  }

  node_iterator node_end() {
    return graph_.node_end();
  }

  edge_iterator edge_begin() {
    return graph_.edge_begin();
  }

  edge_iterator edge_end() {
    return graph_.edge_end();
  }

};
