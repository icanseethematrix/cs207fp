/**
 * @file shallow_water.cpp
 * Implementation of a shallow water system using Mesh
 *
 * @brief Reads in two files specified on the command line.
 * First file: 3D point list (one per line) defined by three doubles
 * Second file: Triangles (one per line) defined by 3 indices into the point list
 */

#include <fstream>
#include <cmath>

#include "CS207/SDLViewer.hpp"
#include "CS207/Util.hpp"
#include "CS207/Color.hpp"

#include "Point.hpp"
#include "Mesh.hpp"

// Standard gravity (average gravity at Earth's surface) in meters/sec^2
static constexpr double grav = 9.80665;

/** Water column characteristics */
struct QVar {
  double h;   // Height of column
  double hx;  // Height times average x velocity of column
  double hy;  // Height times average y velocity of column

  /** Default constructor.
   *
   * A default water column is 1 unit high with no velocity. */
  QVar()
      : h(1), hx(0), hy(0) {
  }
  /** Construct the given water column. */
  QVar(double h_, double hx_, double hy_)
      : h(h_), hx(hx_), hy(hy_) {
  }
  
 /**Add scalar @a b to this Qvar*/
  QVar& operator+=(const QVar b) {
      h  += b.h;
      hx += b.hx;
      hy += b.hy;
      return *this;
  }
  /**Subtract scalar @a b from this QVar */
  QVar& operator-=(const QVar b) {
      h  -= b.h;
      hx -= b.hx;
      hy -= b.hy;
      return *this;
  }
  /** Scale this QVar up by scalar @a b */
  QVar& operator*=(double b) {
      h  *= b;
      hx *= b;
      hy *= b;
      return *this;
  }
  /** Scale this QVar down by scalar @a b */
  QVar& operator/=(double b) {
      h  /= b;
      hx /= b;
      hy /= b;
      return *this;
  }
};

  /** Add QVar @a b to this QVar */
  QVar operator+(QVar a, const QVar& b) {
      return a += b;
  }

  QVar operator/(QVar a, double b) {
      return a /= b;
  }

  QVar operator-(QVar a, const QVar& b) {
      return a -= b;
  }

  QVar operator*(double b, QVar a) {
      return a *= b;
  }

  QVar operator*(QVar a, double b) {
      return a *= b;
  }


struct NodeData{
  QVar q;
  double b;
};

struct EdgeData{
  float flux;
};

struct TriData{
  QVar q_bar;
};

/** Water column characteristics - Added for Final Project*/
struct newFObject {
  double F;   // Force in the z-direction 
  double A;   // Area 
  double ro;  // fluid density

  /** Default constructor.
   *
   * A default floating Obj has no force on water */
  newFObject() : F(0), A(1), ro(1000) {}

  //** Constructer **/
  newFObject(double F_, double A_, double ro_): F(F_), A(A_), ro(ro_) {}
};

// HW4B: Placeholder for Mesh Type!
// Define NodeData, EdgeData, TriData, etc
// or redefine for your particular Mesh
typedef Mesh<NodeData,EdgeData,TriData> MeshType;


/** Function object for calculating shallow-water flux.
 *          |n
 *   T_k    |---> n = (nx,ny)   T_m
 *   QBar_k |                   QBar_m
 *          |
 * @param[in] nx, ny Defines the 2D outward normal vector n = (@a nx, @a ny)
 *            from triangle T_k to triangle T_m. The length of n is equal to the
 *            the length of the edge, |n| = |e|.
 * @param[in] dt The time step taken by the simulation. Used to compute the
 *               Lax-Wendroff dissipation term.
 * @param[in] qk The values of the conserved variables on the left of the edge.
 * @param[in] qm The values of the conserved variables on the right of the edge.
 * @return The flux of the conserved values across the edge e
 */
struct EdgeFluxCalculator {
  QVar operator()(double nx, double ny, double dt,
                  const QVar& qk, const QVar& qm, newFObject obj = newFObject()) {
    // Normalize the (nx,ny) vector
    double n_length = std::sqrt(nx*nx + ny*ny);
    nx /= n_length;
    ny /= n_length;

    // The velocities normal to the edge
    double wm = (qm.hx*nx + qm.hy*ny) / qm.h;
    double wk = (qk.hx*nx + qk.hy*ny) / qk.h;

    // Lax-Wendroff local dissipation coefficient
    double vm = sqrt(grav*qm.h) + sqrt(qm.hx*qm.hx + qm.hy*qm.hy) / qm.h;
    double vk = sqrt(grav*qk.h) + sqrt(qk.hx*qk.hx + qk.hy*qk.hy) / qk.h;
    double a  = dt * std::max(vm*vm, vk*vk);

    // Helper values
    double scale = 0.5 * n_length;
    double gh2   = 0.5 * grav * (qm.h*qm.h + qk.h*qk.h);

    // Simple flux with dissipation for stability
    return QVar(scale * (wm*qm.h  + wk*qk.h)           - a * (qm.h  - qk.h),
                scale * (wm*qm.hx + wk*qk.hx + gh2*nx) - a * (qm.hx - qk.hx),
                scale * (wm*qm.hy + wk*qk.hy + gh2*ny) - a * (qm.hy - qk.hy));
  }
};

// defines an arbitrary force for floating objects-default values for now - added for project
struct ForceCalculator{
  newFObject operator()(){
    return newFObject();
  }
};

/** Node position function object for use in the SDLViewer. */
struct NodePosition {
  template <typename NODE>
  Point operator()(const NODE& n) {
    return Point(n.position().x, n.position().y, n.value().q.h);
  }
};


/** Integrate a hyperbolic conservation law defined over the mesh m
 * with flux functor f by dt in time.
 */
template <typename MESH, typename FLUX, typename FORCE>
double hyperbolic_step(MESH& m, FLUX& f, FORCE& force, double t, double dt) {
  // HW4B: YOUR CODE HERE
  // Step the finite volume model in time by dt.

  // Pseudocode:
  // Compute all fluxes. (before updating any triangle Q_bars)
  // For each triangle, update Q_bar using the fluxes as in Equation 8.
  //  NOTE: Much like symp_euler_step, this may require TWO for-loops

  //iterate over all triangles
  for (auto triangleitr = m.triangle_begin(); triangleitr != m.triangle_end(); ++triangleitr) {
    QVar sum = QVar(0,0,0);

    // Defines the force of the floating objects
    newFObject obj = force();

    MeshType::Triangle newTriangle = (*triangleitr);
    
    Point normalvec = (newTriangle).normal((newTriangle).node1(), (newTriangle).node2());

    // Calcuate flux using adjacent triangle's Q
    if ((newTriangle).adjTriangle((newTriangle).node1(), (newTriangle).node2()).is_valid()){
      sum += f(normalvec.x, normalvec.y, dt, (newTriangle).value().q_bar, (newTriangle).adjTriangle((newTriangle).node1(), (newTriangle).node2()).value().q_bar, obj);
    }else{
      // BOundary conditions
      sum += f(normalvec.x, normalvec.y, dt, (newTriangle).value().q_bar, QVar((newTriangle).value().q_bar.h, 0, 0), obj);
    }

    // Repeat for remaining two edges
    normalvec = (newTriangle).normal((newTriangle).node3(), (newTriangle).node1());
    if ((newTriangle).adjTriangle((newTriangle).node3(), (newTriangle).node1()).is_valid()){
      sum += f(normalvec.x, normalvec.y, dt, (newTriangle).value().q_bar, 
          (newTriangle).adjTriangle((newTriangle).node3(), (newTriangle).node1()).value().q_bar, obj); 
    }else{
      sum += f(normalvec.x, normalvec.y, dt, (newTriangle).value().q_bar, 
          QVar((newTriangle).value().q_bar.h, 0, 0), obj);
    }

    normalvec = (newTriangle).normal((newTriangle).node2(), (newTriangle).node3());
    if ((newTriangle).adjTriangle((newTriangle).node2(), (newTriangle).node3()).is_valid()){
      sum += f(normalvec.x, normalvec.y, dt, (newTriangle).value().q_bar, 
          (newTriangle).adjTriangle((newTriangle).node2(), (newTriangle).node3()).value().q_bar, obj); 
    }else {
      sum += f(normalvec.x, normalvec.y, dt, (newTriangle).value().q_bar, 
          QVar((newTriangle).value().q_bar.h, 0, 0), obj);
    }

    (newTriangle).value().q_bar =(newTriangle).value().q_bar - (sum * dt/(newTriangle).area());
  }

  // update q_bar values for all triangles
  for (auto triangleitr = m.triangle_begin(); triangleitr != m.triangle_end(); ++triangleitr)
    (*triangleitr).value().q_bar = (*triangleitr).value().q_bar;

  return t + dt;
}

/** Convert the triangle-averaged values to node-averaged values for viewing. */
template <typename MESH>
void post_process(MESH& m) {
  // HW4B: Post-processing step
  // Translate the triangle-averaged values to node-averaged values
  // Implement Equation 9 from your pseudocode here
  for (auto nodeitr = m.node_begin(); nodeitr != m.node_end(); ++nodeitr) {
    QVar value = QVar(0,0,0);
    double areasum = 0;
    for (auto triangleitr = m.adj_triangle_begin((*nodeitr).uid()); triangleitr != m.adj_triangle_end((*nodeitr).uid()); ++triangleitr) {
      value += (*triangleitr).value().q_bar * (*triangleitr).area();
      areasum += (*triangleitr).area();
    }

    (*nodeitr).value().q = value * 1.0/(areasum);
  }
}

struct bMet {
  double operator()(Point p, double factor) {
    return p.x/(factor*10);
    // return 0;
  }
};

/* Initial condition*/
struct Wave {
  QVar operator()(Point p) {
    if (pow(p.x-0.75,2) + pow(p.y,2) - pow(0.15,2) < 0) return QVar(1.75,0,0);
    else return QVar(1.0,0,0);
  }
};

/* Initial condition*/
struct Dam {
  QVar operator()(Point a) {
    if (a.x < 0) return QVar(1.75, 0, 0);
    else return QVar(1.0, 0, 0);
  }
};

/* Initial condition*/
struct Pebble{
  QVar operator()(Point p) {
    return QVar(1.0 - 0.75 * exp(-80*(pow((p.x-0.75),2) + pow(p.y,2))), 0, 0);
  }
};



int main(int argc, char* argv[])
{
  // Check arguments
  if (argc < 3) {
    std::cerr << "Usage: shallow_water NODES_FILE TRIS_FILE\n";
    exit(1);
  }

  MeshType mesh;

  std::vector<typename MeshType::node_type> mesh_node;

  // Read all Points and add them to the Mesh
  std::ifstream nodes_file(argv[1]);
  Point p;
  while (CS207::getline_parsed(nodes_file, p)) {
    mesh_node.push_back(mesh.add_node(p));
  }

  // Read all mesh triangles and add them to the Mesh
  std::ifstream tris_file(argv[2]);
  std::array<int,3> t;
  while (CS207::getline_parsed(tris_file, t)) {
    // HW4B: Need to implement add_triangle before this can be used!
    mesh.add_triangle(mesh_node[t[0]], mesh_node[t[1]], mesh_node[t[2]]);
  }

  // Print out the stats
  std::cout << mesh.num_nodes() << " "
            << mesh.num_edges() << " "
            << mesh.num_triangles() << std::endl;
  
  double dx = 0;
  double dy = 0;
  double max_h = 0;
  auto init_cond = Pebble();
  auto secondCond = bMet(); 


  // Find the maximum height and apply initial conditions to nodes
  for (auto iter = mesh.node_begin(); iter != mesh.node_end(); ++iter) { 
    auto n = *iter;
    n.value().q = init_cond(n.position());
    n.value().b = secondCond(n.position(), mesh.num_nodes());
    max_h = std::max(max_h, n.value().q.h);
  } 

  // Set the initial values of the triangles to the average of their nodes 
  for (auto it = mesh.triangle_begin(); it != mesh.triangle_end(); ++it) {
    auto t = *it; 
    t.value().q_bar = (t.node1().value().q + 
                       t.node2().value().q + 
                       t.node3().value().q) / 3.0;
  }

  // Calculate the minimum edge length
  double min_edge_length = std::numeric_limits<double>::max();
  for (auto it = mesh.edge_begin(); it != mesh.edge_end(); ++it) { 
    min_edge_length = std::min(min_edge_length, (*it).length());
  } 


  // Launch the SDLViewer
  CS207::SDLViewer viewer;
  viewer.launch();

  auto node_map = viewer.empty_node_map(mesh);
  viewer.add_nodes(mesh.node_begin(), mesh.node_end(),
                   CS207::DefaultColor(), NodePosition(), node_map);
  viewer.add_edges(mesh.edge_begin(), mesh.edge_end(), node_map);
  viewer.center_view();

  double dt = 0.25 * min_edge_length / (sqrt(grav * max_h));
  double t_start = 0;
  double t_end = 10;

  // Preconstruct a Flux functor
  EdgeFluxCalculator f;

  ForceCalculator Force;
  // Begin the time stepping
  for (double t = t_start; t < t_end; t += dt) {
    // Step forward in time with forward Euler
    hyperbolic_step(mesh, f, Force, t, dt);

    post_process(mesh);

    viewer.add_nodes(mesh.node_begin(), mesh.node_end(),
                     CS207::DefaultColor(), NodePosition(), node_map);
    viewer.set_label(t);

    if (mesh.num_nodes() < 100)
      CS207::sleep(0.05);
  }

  return 0;
}
