/**
 * @file shortest_path.cpp
 * Test script for using our templated Graph to determine shortest paths.
 *
 * @brief Reads in two files specified on the command line.
 * First file: 3D Points (one per line) defined by three doubles
 * Second file: Tetrahedra (one per line) defined by 4 indices into the point
 * list
 */

#include <vector>
#include <fstream>
#include <queue>

#include "CS207/SDLViewer.hpp"
#include "CS207/Util.hpp"
#include "CS207/Color.hpp"

#include "Graph.hpp"


/** Comparator that compares the distance from a given point p.
 */
struct MyComparator {
   Point p_;
   MyComparator(const Point& p) : p_(p) {
   };

   template <typename NODE>
   bool operator()(const NODE& node1, const NODE& node2) const {
    return norm(node1.position() - p_) < norm(node2.position() - p_);
  }
};


/** Calculate shortest path lengths in @a g from the nearest node to @a point.
 * @param[in,out] g Input graph
 * @param[in] point Point to find the nearest node to.
 * @post Graph has modified node values indicating the minimum path length
 *           to the nearest node to @a point
 * @post Graph nodes that are unreachable to the nearest node to @a point have
 *           the value() -1.
 * @return The maximum path length found.
 *
 * Finds the nearest node to @a point and treats that as the root node for a
 * breadth first search.
 * This sets node's value() to the length of the shortest path to
 * the root node. The root's value() is 0. Nodes unreachable from
 * the root have value() -1.
 */
int shortest_path_lengths(Graph<int, int>& g, const Point& point) {
  typedef Graph<int, int> GraphType;
  typedef GraphType::node_type Node;

  //find root
  auto rootNode = std::min_element(g.node_begin(), g.node_end(), MyComparator(point));
  (*rootNode).value() = 0;
  //initialize bfs queue
  std::queue<Node> nodeQueue;
  
  // initialize maxDistance to 0
  int maxDistance = 0;

  // add root to nodeQueue
  nodeQueue.push(*rootNode);

  // declare current and next node
  Node currentNode;
  Node nextNode;

  //bfs
  while(!nodeQueue.empty()){
    currentNode = nodeQueue.front();
    // iterate over all incident edges
    for (auto itr = currentNode.edge_begin(); itr != currentNode.edge_end(); ++itr){
      if ((*itr).node1() == currentNode)
        nextNode = (*itr).node2();
      else
        nextNode = (*itr).node1();

      if (nextNode.value() == 0){
        nextNode.value() = currentNode.value() + 1;
        if (nextNode.value() > maxDistance)
          maxDistance = nextNode.value();
        nodeQueue.push(nextNode);
      }
    }
    nodeQueue.pop();
  }

  //change the value of nodes not found to -1
  for (auto itr = g.node_begin(); itr != g.node_end(); ++itr){
    if ((*itr).value() == 0)
      (*itr).value() = -1;
  }

  return maxDistance;
}

// calls the color map
struct ColorFunction {
  int d_;
  ColorFunction(int d) : d_(d) {}
  CS207::Color operator()(const Graph<int, int>::node_type n) {
    return CS207::Color::make_heat(static_cast<float> (n.value())/d_);
  }
};




int main(int argc, char** argv)
{
  // Check arguments
  if (argc < 3) {
    std::cerr << "Usage: " << argv[0] << " NODES_FILE TETS_FILE\n";
    exit(1);
  }

  // Construct a Graph
  typedef Graph<int, int> GraphType;
  GraphType graph;
  std::vector<GraphType::node_type> nodes;

  // Create a nodes_file from the first input argument
  std::ifstream nodes_file(argv[1]);
  // Interpret each line of the nodes_file as a 3D Point and add to the Graph
  Point p;
  while (CS207::getline_parsed(nodes_file, p))
    nodes.push_back(graph.add_node(p));

  // Create a tets_file from the second input argument
  std::ifstream tets_file(argv[2]);
  // Interpret each line of the tets_file as four ints which refer to nodes
  std::array<int,4> t;
  while (CS207::getline_parsed(tets_file, t))
    for (unsigned i = 1; i < t.size(); ++i)
      for (unsigned j = 0; j < i; ++j)
        graph.add_edge(nodes[t[i]], nodes[t[j]]);

  // Print out the stats
  std::cout << graph.num_nodes() << " " << graph.num_edges() << std::endl;

  // Launch the SDLViewer
  CS207::SDLViewer viewer;
  viewer.launch();


  auto node_map = viewer.empty_node_map(graph);
  int maxDistance = shortest_path_lengths(graph, {-1,0,1});
  viewer.add_nodes(graph.node_begin(), graph.node_end(), ColorFunction(maxDistance), node_map);
  viewer.add_edges(graph.edge_begin(), graph.edge_end(), node_map);
  return 0;
}
