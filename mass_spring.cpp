/**
 * @file mass_spring.cpp
 * Implementation of mass-spring system using Graph
 *
 * @brief Reads in two files specified on the command line.
 * First file: 3D Points (one per line) defined by three doubles
 * Second file: Tetrahedra (one per line) defined by 4 indices into the point
 * list
 */

#include <fstream>

#include "CS207/SDLViewer.hpp"
#include "CS207/Util.hpp"
#include "CS207/Color.hpp"

#include "Graph.hpp"
#include "Point.hpp"


// Gravity in meters/sec^2
static constexpr double grav = 9.81;

/** Custom structure of data to store with Nodes */
struct NodeData {
  Point velocity;  //< Node velocity
  double mass;     //< Node mass
};

/** Custom structure of data to store with EdgeData */
struct EdgeData {
  double K;
  double L;
};

// HW2 #1 YOUR CODE HERE
// Define your Graph type
typedef Graph<NodeData, EdgeData> GraphType;
typedef typename GraphType::node_type Node;
typedef typename GraphType::edge_type Edge;

struct NullOp {
  template <typename P1, typename P2>
  void operator()(P1&, P2&) {}
};

/** Change a graph's nodes according to a step of the symplectic Euler
 *    method with the given node force.
 * @param[in,out] g      Graph
 * @param[in]     t      The current time (useful for time-dependent forces)
 * @param[in]     dt     The time step
 * @param[in]     force  Function object defining the force per node
 * @return the next time step (usually @a t + @a dt)
 *
 * @tparam G::node_value_type supports ???????? YOU CHOOSE
 * @tparam F is a function object called as @a force(n, @a t),
 *           where n is a node of the graph and @a t is the current time.
 *           @a force must return a Point representing the force vector on Node
 *           at time @a t.
 */
template <typename G, typename F, typename C = NullOp>
double symp_euler_step(G& g, double t, double dt, F force, C constraint = NullOp()) {
  // Compute the {n+1} node positions
  for (auto it = g.node_begin(); it != g.node_end(); ++it) {
    auto n = *it;

    // Update the position of the node according to its velocity
    // x^{n+1} = x^{n} + v^{n} * dt
    //n.position() += n.value().velocity * dt;
    n.set_position(n.position() + (n.value().velocity * dt));
  }

  constraint(g, t);
  // Compute the {n+1} node velocities
  for (auto it = g.node_begin(); it != g.node_end(); ++it) {
    auto n = *it;
    // v^{n+1} = v^{n} + F(x^{n+1},t) * dt / m
    n.value().velocity += force(n, t) * (dt / n.value().mass);
    if (n.position() == Point(0,0,0) || n.position() == Point(1, 0, 0)){
      n.value().velocity = Point(0,0,0);
    }
  }

  return t + dt;
}


/** Force function object for HW2 #1. */
struct Problem1Force {
  /** Return the force being applied to @a n at time @a t.
   *
   * For HW2 #1, this is a combination of mass-spring force and gravity,
   * except that points at (0, 0, 0) and (1, 0, 0) never move. We can
   * model that by returning a zero-valued force. */
  Point operator()(Node n, double t) {
    // HW2 #1: YOUR CODE HERE
    Point position = n.position();

    if (position == Point(0,0,0) || position == Point(1, 0, 0)){
      return Point(0,0,0);
    }

    Point sForce = Point(0., 0., 0.);

    double X, Y;

    for (auto it = n.edge_begin(); it != n.edge_end(); ++it) {
      Point x1 = (*it).node2().position();
      X = (*it).value().L;
      Y = (*it).value().K;
      //X = 0.25;
      //Y = 100.0;
      sForce = sForce - Y * (position - x1)/norm(position - x1) * (norm(position - x1) - X);
    }

    Point gForce = Point(0., 0., -n.value().mass * grav);

    (void) n; (void) t;     // silence compiler warnings
    return sForce + gForce;
  }
};

struct GravityForce {
  Point operator()(Node n, double t) {
    Point gForce = Point(0., 0., -n.value().mass * grav);
    (void) t;
    return gForce;
  }
};

struct MassSpringForce {
    Point operator()(Node n, double t) {
    // HW2 #1: YOUR CODE HERE
      Point position = n.position();

      Point sForce = Point(0., 0., 0.);

      double X, Y;

      for (auto it = n.edge_begin(); it != n.edge_end(); ++it) {
        Point x1 = (*it).node2().position();
        //X = (*it).value().L;
        //Y = (*it).value().K;
        X = .25;
        Y = 100.;
        sForce = sForce - Y * (position - x1)/norm(position - x1) * (norm(position - x1) - X);
        //printf("%d %d\n", (*it).value(), Y);
        //break;
      }

      (void) t;
      return sForce;
  }
};

struct DampingForce {
  Point operator()(Node n, double t) {
    Point dForce = -c_ * n.value().velocity;
    (void) t;
    return dForce;
  }

  double c_;

  DampingForce(double c) :c_(c){}
};

template<typename f1, typename f2>
struct combinedForce
{
  Point operator()(Node n, double t){
    return force1_(n, t) + force2_(n, t);
  }

  combinedForce(f1 force1, f2 force2): force1_(force1), force2_(force2){}
  
  f1 force1_;
  f2 force2_;
};

template<typename f1, typename f2>
combinedForce<f1, f2> make_combined_force(f1 force1, f2 force2){
  return combinedForce<f1, f2>(force1, force2);
}

template<typename f1, typename f2, typename f3>
combinedForce<f1, combinedForce<f2, f3>> make_combined_force(f1 force1, f2 force2, f3 force3){
  combinedForce <f2, f3> tempForce = combinedForce<f2, f3>(force2, force3);
  return combinedForce <f1, combinedForce<f2, f3>> (force1, tempForce);
}

struct PlaneConstraint{
  void operator()(GraphType g, double t){
    for (auto iter = g.node_begin(); iter != g.node_end(); ++iter){
      Node x = *iter;
      Point position = x.position();

      if(position[axis_] < constraint_){
        position[axis_] = constraint_;
        x.set_position(position);
      }
    }
    (void)t;
  }

  PlaneConstraint(int axis, double constraint) : axis_(axis), constraint_(constraint){}  

  int axis_;
  double constraint_;

};


struct SphereConstraint
{
  void operator()(GraphType& g, double t) {
    (void) t;
    for (auto it = g.node_begin(); it != g.node_end(); ++it){
      Node currentNode = *it;
      Point position = currentNode.position();
      double distance = norm(position - centre_);
      if (distance < radius_){
        Point unitVector = (position - centre_)/distance;
        currentNode.set_position(centre_ + unitVector*radius_);
        currentNode.value().velocity = currentNode.value().velocity - dot(currentNode.value().velocity, unitVector) * unitVector;
      }
    }
  }

  SphereConstraint(Point centre, double radius): centre_(centre), radius_(radius){}

  Point centre_;
  double radius_;
};

struct SphereConstraintRemove{
  void operator()(GraphType& g, double t) {
    (void) t;
    for (auto it = g.node_begin(); it != g.node_end(); ++it){
      Node currentNode = *it;
      Point position = currentNode.position();
      double distance = norm(position - centre_);
      if (distance < radius_){
        for (auto it2 = currentNode.edge_begin(); it2 != currentNode.edge_end(); ++it2) {
          Edge currentEdge = *it2;
          g.remove_edge(currentEdge);
        }
        g.remove_node(currentNode);
      }
    }
  }

  SphereConstraintRemove(Point centre, double radius): centre_(centre), radius_(radius){}

  Point centre_;
  double radius_;

};

template<typename cst1, typename cst2>
struct combinedConstraint{
  cst1 constraint1_;
  cst2 constraint2_;

  combinedConstraint(cst1 constraint1, cst2 constraint2): constraint1_(constraint1), constraint2_(constraint2){}

  void operator()(GraphType& g, double t){
    constraint1_(g, t);
    constraint2_(g, t);
  }
};

template <typename cst1, typename cst2>
combinedConstraint<cst1, cst2> make_combined_constraint(cst1 constraint1, cst2 constraint2){
  return combinedConstraint<cst1, cst2>(constraint1, constraint2);
}

int main(int argc, char** argv) {
  // Check arguments
  if (argc < 3) {
    std::cerr << "Usage: " << argv[0] << " NODES_FILE TETS_FILE\n";
    exit(1);
  }

  // Construct a graph
  GraphType graph;

  // Create a nodes_file from the first input argument
  std::ifstream nodes_file(argv[1]);
  // Interpret each line of the nodes_fie as a 3D Point and add to the Graph
  std::vector<Node> nodes;
  Point p;
  while (CS207::getline_parsed(nodes_file, p))
    nodes.push_back(graph.add_node(p));

  // Create a tets_file from the second input argument
  std::ifstream tets_file(argv[2]);
  // Interpret each line of the tets_file as four ints which refer to nodes
  std::array<int,4> t;
  while (CS207::getline_parsed(tets_file, t)) {
    for (unsigned i = 1; i < t.size(); ++i) {
      graph.add_edge(nodes[t[0]], nodes[t[1]]);
      graph.add_edge(nodes[t[0]], nodes[t[2]]);
      // Diagonal edges: include as of HW2 #2
      graph.add_edge(nodes[t[0]], nodes[t[3]]);
      graph.add_edge(nodes[t[1]], nodes[t[2]]);
      graph.add_edge(nodes[t[1]], nodes[t[3]]);
      graph.add_edge(nodes[t[2]], nodes[t[3]]);
    }
  }

  // HW2 #1 YOUR CODE HERE
  // Set initial conditions for your nodes, if necessary.
  // Construct Forces/Constraints

  Point initialVelocity = Point(0., 0., 0.);
  double initialMass = 1./double(graph.num_nodes());

  for (auto it = graph.node_begin();  it != graph.node_end(); ++it){
    (*it).value().mass = initialMass;
    (*it).value().velocity = initialVelocity;
  }

  for (auto it = graph.edge_begin(); it != graph.edge_end(); ++it){
    (*it).value().K = 100;
    (*it).value().L = (*it).length();
  }

  for (auto it = graph.edge_begin();  it != graph.edge_end(); ++it){
    printf("%d\n", (*it).value().K); 
    printf("%d\n", (*it).value().L); ;
  }


  // Print out the stats
  std::cout << graph.num_nodes() << " " << graph.num_edges() << std::endl;

  // Launch the SDLViewer
  CS207::SDLViewer viewer;
  auto node_map = viewer.empty_node_map(graph);
  viewer.launch();

  viewer.add_nodes(graph.node_begin(), graph.node_end(), node_map);
  viewer.add_edges(graph.edge_begin(), graph.edge_end(), node_map);

  viewer.center_view();

  // Begin the mass-spring simulation
  double dt = 0.001;
  double t_start = 0.0;
  double t_end   = 5.0;

  for (double t = t_start; t < t_end; t += dt) {
    //std::cout << "t = " << t << std::endl;

    auto forces = make_combined_force(MassSpringForce(), GravityForce(),  DampingForce(1./float(graph.size())));
    auto constraints = SphereConstraintRemove(Point(0.5,0.5,-0.5),0.15);
    symp_euler_step(graph, t, dt, forces, constraints);

    

    // Update viewer with nodes' new positions
    viewer.add_nodes(graph.node_begin(), graph.node_end(), node_map);
    viewer.set_label(t);

    // These lines slow down the animation for small graphs, like grid0_*.
    // Feel free to remove them or tweak the constants.
    if (graph.size() < 100)
      CS207::sleep(0.001);
  }

  return 0;
}
