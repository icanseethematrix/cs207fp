/**
 * @file mtl_test.cpp
 * Test script for interfacing with MTL4 and it's linear solvers.
 */

// HW3: Need to install/include Boost and MTL in Makefile
#include <boost/numeric/mtl/mtl.hpp>
#include <boost/numeric/itl/itl.hpp>

using namespace mtl;
using namespace itl;

// Define a IdentityMatrix that interfaces with MTL
struct IdentityMatrix {
	/** Compute the product of a vector with this identity matrix*/
	IdentityMatrix(int n):n(n){}

	/** Helper function to perform multiplication . Allows for delayed
	* evaluation of results .
	* Assign :: apply (a, b) resolves to an assignment operation such as
	* a += b, a -= b, or a = b.
	* @pre @a size (v) == size (w) */
	template <typename VectorIn , typename VectorOut , typename Assign >
	void mult ( const VectorIn & v, VectorOut & w, Assign ) const{
		//make sure multiplication is valid
		assert(int(size(v)) == n);
		assert(size(v) == size(w));

		for (int i = 0; i < size(v); i++){
			w[i] = Assign::apply(w[i], v[i]);
		}
	}

	/** Matrix - vector multiplication forwards to MTL ’s lazy mat_cvec_multiplier operator */
	template <typename Vector>
	mtl::vec::mat_cvec_multiplier<IdentityMatrix, Vector>
	operator *(const Vector& v) const {
		return mtl::vec::mat_cvec_multiplier<IdentityMatrix, Vector>(* this , v);
	}

	// number of rowns and columns in square matrix
	int n;

private:
};

/** The number of elements in the matrix . */
inline std :: size_t size ( const IdentityMatrix & A) {
	return A.n * A.n;
}

/** The number of rows in the matrix . */
inline std :: size_t num_rows ( const IdentityMatrix & A) {
	return A.n;
}

/** The number of columns in the matrix . */
inline std :: size_t num_cols ( const IdentityMatrix & A) {
	A.n;
}

/** Traits that MTL uses to determine properties of our IdentityMatrix . */
namespace mtl {
	namespace ashape {
		/** Define IdentityMatrix to be a non - scalar type . */
		template < >
		struct ashape_aux < IdentityMatrix > {
			typedef nonscal type ;
		};
	} // end namespace ashape

	/** IdentityMatrix implements the Collection concept
	* with value_type and size_type */
	template < >
	struct Collection < IdentityMatrix > {
		typedef double value_type ;
		typedef unsigned size_type ;
	};
} // end namespace mtl



int main()
{
  	// Construct an IdentityMatrix and "solve" Ix = b
  	// using MTL's conjugate gradient solver

	const int size = 40;
    typedef IdentityMatrix matrix_type;

    // Set up a matrix n x n
    matrix_type A(size);

    // Create an ILU(0) preconditioner
    itl::pc::identity<matrix_type>     P(A);
    
    // Set b such that x == 1 is solution; start with x == 0
    mtl::dense_vector<double>          x(size, 1.0), b(size);
    b= A * x; x= 0;
    
    // Termination criterion: r < 1e-6 * b or N iterations
    noisy_iteration<double>       iter(b, 500, 1.e-6);
    
    // Solve Ax == b with left preconditioner P
    cg(A, x, b, P, iter);

    return 0;
}
