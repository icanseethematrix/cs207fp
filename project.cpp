/**
 * @file test_tet_mesh.cpp
 * Tetrahedral Mesh testing file
 *
 * @brief Reads in two files specified on the command line.
 * First file: 3D point list (one per line) defined by three doubles
 * Second file: Tetrahedrals (one per line) defined by 4 indices into the point list
 */

#include <fstream>
#include <cmath>
using std::pow;
using std::exp;
#include <iostream>
using std::cout;
using std::endl;
#include <map>
using std::map;
#include <string>
using std::string;

#include "CS207/SDLViewer.hpp"
#include "CollisionDetector.hpp"
#include "CS207/Util.hpp"
#include "CS207/Color.hpp"
#include "CS207/Point.hpp"

#include "tet_mesh.hpp"


//DEBUG
//#define TETRAHEDRAL_OUTPUT
//#define DEBUG_MESH

struct NodeData;
struct EdgeData;
struct TetrahedralData;
struct MakeSphere;
typedef CS207::SDLViewer Viewer;
typedef Mesh<NodeData, EdgeData, TetrahedralData> MeshType;
typedef MeshType::size_type size_type;
typedef typename MeshType::Node Node;
typedef typename MeshType::Edge Edge;
typedef std::map<MeshType::node_type, unsigned> NodeMapType;
typedef CollisionDetector<MeshType> Collider;
typedef typename Collider::Collision Collision;

/** Custom structure of data to store with Nodes */
struct NodeData {
	Point velocity;  //< Node velocity
	double mass;     //< Node mass
};

/** Custom structure of data to store with Edges */
struct EdgeData {
	double length;   //< Edge length
};

/** Custom structure of data to store with Tetrahedral */
struct TetrahedralData {
	double initialVolume;   //< Initial Tetrahedral Volume
};

struct NullConstraint;

/** Change a mesh's nodes according to a step of the symplectic Euler
 *    method with the given node force.
 * @param[in,out] m      Mesh
 * @param[in]     t      The current time (useful for time-dependent forces)
 * @param[in]     dt     The time step
 * @param[in]     force  Function object defining the force per node
 * @param[in]	  constrain Constrain object defining the constraints for the simulation
 *                Defaulted to FixedConstraint where the Points (0,0,0) and (1,0,0) are fixed
 * @return the next time step (usually @a t + @a dt)
 *
 * @tparam G::node_value_type supports ???????? YOU CHOOSE
 * @tparam F is a function object called as @a force(n, @a t),
 *           where n is a node of the mesh and @a t is the current time.
 *           @a force must return a Point representing the force vector on Node
 *           at time @a t.
 */
template <typename M, typename F, typename C = NullConstraint>
double symp_euler_step(M& m, double t, double dt, F force, C constrain = C() ) {

	//Set constraints
	constrain(m, t);

	// Compute the {n+1} node positions
	for (auto it = m.node_begin(); it != m.node_end(); ++it) {
		auto n = *it;

		// Update the position of the node according to its velocity
		// x^{n+1} = x^{n} + v^{n} * dt
		n.position() += n.value().velocity * dt;
	}

	// Compute the {n+1} node velocities
	for (auto it = m.node_begin(); it != m.node_end(); ++it) {
		auto n = *it;
		// v^{n+1} = v^{n} + F(x^{n+1},t) * dt / m
		n.value().velocity += force(n, t) * (dt / n.value().mass);
	}



	return t + dt;
}


/** Dashpot Force Functor that returns the Dashpot Force
 */
struct DashpotForce {
	double K_;
	double C_;
	/** DashpotForce Constructor.
	 * @param[in] K Spring constant in N/m
	 * @param[in] K Damping constant in in N*s/m
	 */
	DashpotForce(double K = 100, double C=100) : K_(K), C_(C) {}

	/** Calculates Dashpot Force
	 * @param[in] n Valid node.
	 * @param[in] t Valid time.
	 * @return Point object that represents the dashpot force.
	 */
	Point operator()(Node n, double t) {
		(void) t;     // silence compiler warnings
		K_ = 1000;
		Point dashpotForce(0,0,0);
		for (auto it = n.edge_begin(); it != n.edge_end(); ++it) {
			Node incidentNode = (*it).node2();
			if( (*it).node1().index() == n.index() ) {
				incidentNode = (*it).node2();
			} else {
				incidentNode = (*it).node1();
			}

			double distance = norm(n.position() - incidentNode.position());
			//Spring component
			double spring_comp = K_ * (distance - (*it).value().length);
			//Damping component
			//Point c_comp = C_* (n.value().velocity - incidentNode.value().velocity) *
			//		       (n.position() - incidentNode.position()) / distance;
			Point unitVector = (n.position() - incidentNode.position()) / distance;

			dashpotForce += (-1.0) * (spring_comp) * unitVector;
		}
		return dashpotForce;
	}
};

/** Volume Penalty Force Functor that returns the Volume Penalty Force
 */
struct VolumePenaltyForce {
	MeshType* m_;
	double K_;
	/** VolumePenaltyForce Constructor.
	 * @param[in] g Gravity in m/s^2.
	 */
	VolumePenaltyForce(MeshType* m, double K) : m_(m), K_(K) {}

	/** Calculates VolmePenalty Force
	 * @param[in] n Valid node.
	 * @param[in] t Valid time.
	 * @return Point object that represents the volume penalty force.
	 */
	Point operator()(Node n, double t) {
		(void) t;     // silence compiler warnings
		Point volumePenaltyForce(0,0,0);
		auto AdjTetrahedral = n.nodeAdjTetrahedral();

		// Loop through all of the node's adj tetrahedrals
		for(unsigned k = 0; k < AdjTetrahedral.size(); ++k) {
			MeshType::Tetrahedral tet = AdjTetrahedral[k];
			// Mass weighted centroid
			Point baryCenter(0,0,0);
			double totalMass = 0;

			// Loop through each of the tet's node to calculate baryCenter
			for(unsigned i = 0; i < NUM_TET_ADJ_TET; ++i) {
				baryCenter += tet.node(i).position() * tet.node(i).value().mass;
				totalMass += tet.node(i).value().mass;
			}
			baryCenter = baryCenter/totalMass;

			Point unitVector = (n.position() - baryCenter)/norm(n.position() - baryCenter);
			volumePenaltyForce += -K_ * (tet.volume() - tet.value().initialVolume) * unitVector;

		}
		return volumePenaltyForce;
	}
};


/** Combine Force Functor that returns a combination of forces
 * @param[in] Two valid forces in f1 and f2.
 */
template <typename Force1, typename Force2>
struct CombineForces {
	Force1 f1_;
	Force2 f2_;

	/** CombineForces Constructor.
	 * @param[in] f1 First valid force.
	 * @param[in] f2 Second valid force.
	 */
	CombineForces(Force1 f1, Force2 f2)
			: f1_(f1), f2_(f2) {}

	/** Calculates Combine Forces
	 * @param[in] n Valid node.
	 * @param[in] t Valid time.
	 * @return Point object that represents the combination of forces of @a f1_ and @a f2_.
	 */

	Point operator() (Node n, double t){
		return f1_(n,t) + f2_(n,t);
	}
};

/** Combine Force Function that returns a combination of forces
 * @param[in] Two valid forces in f1, and f2.
 * @pre Valid forces that input a node and time with a function call of f(n,t).
 * @return A CombineForces object that adds up the forces.
 */
template <typename Force1, typename Force2>
CombineForces<Force1, Force2> make_combined_force(Force1 f1, Force2 f2){
	return CombineForces<Force1, Force2> (f1, f2);
}

/** Combine Force Function that returns a combination of forces
 * @param[in] Three valid forces in f1, f2, and f3.
 * @pre Valid forces that input a node and time with a function call of f(n,t).
 * @return A CombineForces object that adds up the forces.
 */
template <typename Force1, typename Force2, typename Force3>
CombineForces<CombineForces<Force1, Force2>, Force3> make_combined_force(Force1 f1, Force2 f2, Force3 f3){
	return CombineForces<CombineForces<Force1, Force2>, Force3> (CombineForces<Force1, Force2>(f1,f2), f3);
}

/** Box Plane Constraint that models a 5x5 enclosing cube 
 */
struct CubeConstraint {
double constraint_; //coordinates for the cube wall

	/** CubeConstraint Constructor.
	 * @param[in] constraint Sets back,bottom,left corner of the cube to be (constraint,constraint,constraint)
	 */
	CubeConstraint(double constraint) : constraint_(constraint) {}

	/** Constraint Setter
	 * @param[in] g Valid mesh.
	 * @param[in] t Valid time.
	 * @post if any of the nodes in @a mesh have crossed the cube walls the component of the
 	 * 	velocity of all nodes in the @a mesh that is normal to the wall will be negated
	 */
	void operator()(MeshType& m, double t) {
		(void) t;     // silence compiler warnings
		for(auto it = m.node_begin(); it != m.node_end(); ++it) {
			Node n = (*it);

			// Condition for walls on the YZ plane
			if(n.position().x < constraint_ || n.position().x > 5) {
				if (n.position().x>5){
					n.position().x = 5;
				}else{
					n.position().x = constraint_;	
				}
				
				for(auto it = m.node_begin(); it != m.node_end(); ++it){
					Node n1 = (*it);
					n1.value().velocity.x = -n1.value().velocity.x;
				}
				break;
			}

			// Condition for walls on the XZ plane
			if(n.position().y < constraint_ || n.position().y > 5) {
				if (n.position().y>5){
					n.position().y = 5;
				}else{
					n.position().y = constraint_;	
				}
				for(auto it = m.node_begin(); it != m.node_end(); ++it){
					Node n1 = (*it);
					n1.value().velocity.y = -n1.value().velocity.y;
				}
				break;
			}

			// Condition for walls on the XY plane
			if(n.position().z < constraint_ || n.position().z > 5) {
				if (n.position().z>5){
					n.position().z = 5;
				}else{
					n.position().z = constraint_;	
				}
				for(auto it = m.node_begin(); it != m.node_end(); ++it){
					Node n1 = (*it);
					n1.value().velocity.z = -n1.value().velocity.z;
				}
				break;
			}
		}
	}
};

/** Combine Constraints Functor that returns a combination of constraints
 * @param[in] Two valid constraints in c1 and c2.
 */
template <typename Constraint1, typename Constraint2>
struct CombineConstraints {
	Constraint1 c1_;
	Constraint2 c2_;

	CombineConstraints(Constraint1 c1, Constraint2 c2)
			: c1_(c1), c2_(c2) {}

	void operator() (MeshType& m, double t){
		c1_(m,t);
		c2_(m,t);
	}
};

/** Combine Constraints Function that returns a combination of constraints
 * @param[in] Two valid constraints in @a c1, and @a c2.
 * @pre Valid constraints that input a node and time with a function call of c(g,t).
 * @return A CombineConstraints object that executes the constraints.
 */
template <typename Constraint1, typename Constraint2>
CombineConstraints<Constraint1, Constraint2> make_combined_constraints(Constraint1 c1, Constraint2 c2){
	return CombineConstraints<Constraint1, Constraint2> ({c1, c2});
}

/** Combine Constraints Function that returns a combination of constraints
 * @param[in] Valid constraints in @a c1, @a c2, and @a c3.
 * @pre Valid constraints that input a node and time with a function call of c(g,t).
 * @return A CombineConstraints object that executes the constraints.
 */
template <typename Constraint1, typename Constraint2, typename Constraint3>
CombineConstraints<CombineConstraints<Constraint1, Constraint2>, Constraint3> make_combined_constraints(Constraint1 c1, Constraint2 c2, Constraint3 c3){
	return CombineConstraints<CombineConstraints<Constraint1, Constraint2>, Constraint3> ({{c1, c2}, c3});
}

/** Helper function to generate random initial velocities for the spheres 
  *   @param[in] min is the minimum number that should be generated
  *   @param[in] max in the maximum number that should be generated
  */
double get_random(double min, double max) {
  /* Returns a random double between min and max */
  return (max - min) * ( (double)rand() / (double)RAND_MAX ) + min;
}

/** Simulates what will happen when two spheres collide
  * 	For a perfectly ellastic collision, 
  *	the two spheres will simply trade velocities
  */
void bounce_apart(MeshType& mesh1, MeshType& mesh2) {
  // Get before-collision velocities of the spheres
  Point v1 = (*mesh1.node_begin()).value().velocity;
  Point v2 = (*mesh2.node_begin()).value().velocity;

  // Set the velocity of mesh1 to v2
  for (auto it=mesh1.node_begin();it!=mesh1.node_end();++it) {
    (*it).value().velocity = v2;
  }
  // Set the velocity of mesh2 to v1
  for (auto it=mesh2.node_begin();it!=mesh2.node_end();++it) {
    (*it).value().velocity = v1;
  }
}

/** LongClick event handler
  *	Speed up all of the spheres by a factor of 1+duration 
  *	Duration = duration of the click event
  */
struct SpeedUp : public Viewer::EventListener {
  // Constructor
  SpeedUp(MeshType& m1, MeshType& m2) : mesh1(m1), mesh2(m2) {};
  
  void operator()(double duration) {
    if (duration < 0.1) 
      return;
    // Speed up the first sphere 
    for (auto it=mesh1.node_begin();it!=mesh1.node_end();++it) {
      (*it).value().velocity = (*it).value().velocity * (duration+1);
    }
    // Speed up the second sphere
    for (auto it=mesh2.node_begin();it!=mesh2.node_end();++it) {
      (*it).value().velocity = (*it).value().velocity * (duration+1);
    } 
  }

 private:
  MeshType& mesh1;
  MeshType& mesh2;
};

/** RightLongClick event handler
  *	Slow down all of the spheres by a factor of 1+duration 
  *	Duration = duration of the click event
  */
struct SlowDown : public Viewer::EventListener {
  // Constructor
  SlowDown(MeshType& m1, MeshType& m2) : mesh1(m1), mesh2(m2) {};
  
  void operator()(double duration) {
    if (duration < 0.1) 
      return;
    // Speed up the first sphere 
    for (auto it=mesh1.node_begin();it!=mesh1.node_end();++it) {
      (*it).value().velocity = (*it).value().velocity / (duration+1);
    }
    // Speed up the second sphere
    for (auto it=mesh2.node_begin();it!=mesh2.node_end();++it) {
      (*it).value().velocity = (*it).value().velocity / (duration+1);
    } 
  }

 private:
  MeshType& mesh1;
  MeshType& mesh2;
};

/** KeyDown Event handler
  * 	'p' = pause the timer 
  * 	'r' = resume the timer
  */
struct PauseResume : public Viewer::EventListener  {
  // Constructor
  PauseResume(double& t) : dt(t) {};

  void operator()(const char* key) {
    // Fire a pause event by setting dt=0
    if (*key == 'p') {
	dt=0;
    }
    // Fire a resume event by setting dt=0
    if (*key == 'r') {
	dt=0.0005;
    }
  }

 private:
  double& dt;
};

int main(int argc, char* argv[])
{
	// Check arguments
	if (argc < 5) {
		std::cerr << "Usage: " << argv[0] << " NODES_FILE TETS_FILE BOX.nodes BOX.tets\n";
		exit(1);
	}

	cout << "Start" << endl;

  //Collider checker
  Collider c;

  //Declare a vector of meshs and their node_maps
  vector<MeshType> meshes; 
  vector<NodeMapType> node_maps;

  //Declare mesh for the Box
  MeshType box;
  std::vector<typename MeshType::node_type> box_node;

  // Read all Points and add them to the Mesh
  std::ifstream nodes_file_box(argv[3]);
  Point p_box;
  while (CS207::getline_parsed(nodes_file_box, p_box)) {
      //Add nodes
      box_node.push_back(box.add_node(p_box, NodeData()));
  }

  // Read all mesh triangles and add them to the Mesh
  std::ifstream tris_file_box(argv[4]);
  std::array<int,4> t_box;
  while (CS207::getline_parsed(tris_file_box, t_box)) {
	//Initialize each tetrahedral's initial value to be the average of its nodes
    box.add_tetrahedral(box_node[t_box[0]], box_node[t_box[1]], box_node[t_box[2]], box_node[t_box[3]]);
  }

  // Launch the SDLViewer
  Viewer viewer;
  viewer.launch();

  // Add the box to the viewer
  auto node_map_box = viewer.empty_node_map(box);

  viewer.add_nodes(box.node_begin(), box.node_end(), node_map_box);
  viewer.add_edges(box.edge_begin(), box.edge_end(), node_map_box);

  //Declare first mesh
  MeshType mesh;
  std::vector<typename MeshType::node_type> mesh_node;

  //Declare second mesh
  MeshType mesh2;
  std::vector<typename MeshType::node_type> mesh_node2;

  // Read all Points and add them to the Mesh
  Point p;
  std::ifstream nodes_file(argv[1]);
  while (CS207::getline_parsed(nodes_file, p)) {
	  //Add nodes
      mesh_node.push_back(mesh.add_node(p, NodeData()));
      mesh_node2.push_back(mesh2.add_node(p, NodeData()));
//      cout << p << endl;
  }

  // Read all mesh triangles and add them to the Mesh
  std::array<int,4> t;
  std::ifstream tris_file(argv[2]);
  while (CS207::getline_parsed(tris_file, t)) {
	//Initialize each tetrahedral's initial value to be the average of its nodes
    mesh.add_tetrahedral(mesh_node[t[0]], mesh_node[t[1]], mesh_node[t[2]], mesh_node[t[3]]);
    mesh2.add_tetrahedral(mesh_node2[t[0]], mesh_node2[t[1]], mesh_node2[t[2]], mesh_node2[t[3]]);
  }

  // Initialization of mass and length
  // Set initial conditions for your nodes, if necessary.
  // Construct Forces/Constraints

  //Random initial velocity
  //Set mass
  Point vel = Point(get_random(-10,10),get_random(-10,10),get_random(-10,10));
std::cout<<vel<<std::endl;
  Point offset = Point(2,2,2);
  for (auto it = mesh.node_begin(); it != mesh.node_end(); ++it) {
	  auto n = *it;
          n.position() = n.position() + offset;
	  n.value().velocity = vel;
	  n.value().mass = 1.0/mesh.num_nodes();
	}

  //To set rest length for all of the Edges to their initial length
  for (auto ei = mesh.edge_begin(); ei != mesh.edge_end(); ++ei ) {
	  (*ei).value().length = (*ei).length();
  }

  //To set initial Volume for all of the Tetrahedral to their initial Volume
  for (auto ti = mesh.tetrahedral_begin(); ti != mesh.tetrahedral_end(); ++ti ) {
	  (*ti).value().initialVolume = (*ti).volume();
  }

  auto node_map = viewer.empty_node_map(mesh);
  viewer.add_nodes(mesh.node_begin(), mesh.node_end(), node_map);
  viewer.add_edges(mesh.edge_begin(), mesh.edge_end(), node_map);  



  // Read all Points and add them to the Mesh
  Point p2;
  std::ifstream nodes_file2(argv[1]);
  while (CS207::getline_parsed(nodes_file2, p2)) {
	  //Add nodes
      mesh_node2.push_back(mesh2.add_node(p2, NodeData()));
//      cout << p << endl;
  }

  // Initialization of mass and length
  // Set initial conditions for your nodes, if necessary.
  // Construct Forces/Constraints

  //Zero initial velocity
  //Set mass
  Point vel2 = Point(get_random(-10,10),get_random(-10,10),get_random(-10,10));
std::cout<<vel2<<std::endl;
  for (auto it = mesh2.node_begin(); it != mesh2.node_end(); ++it) {
	  auto n = *it;
	  n.value().velocity = vel2;
	  n.value().mass = 1.0/mesh2.num_nodes();
	}

  //To set rest length for all of the Edges to their initial length
  for (auto ei = mesh2.edge_begin(); ei != mesh2.edge_end(); ++ei ) {
	  (*ei).value().length = (*ei).length();
  }

  //To set initial Volume for all of the Tetrahedral to their initial Volume
  for (auto ti = mesh2.tetrahedral_begin(); ti != mesh2.tetrahedral_end(); ++ti ) {
	  (*ti).value().initialVolume = (*ti).volume();
  }

  auto node_map2 = viewer.empty_node_map(mesh2);
  viewer.add_nodes(mesh2.node_begin(), mesh2.node_end(), node_map2);
  viewer.add_edges(mesh2.edge_begin(), mesh2.edge_end(), node_map2);

  viewer.center_view();

  // Add meshes to the collider
  c.add_object(mesh);
  c.add_object(mesh2);


  // Define the time steps
  double dt = 0.0005;
  double t_start = 0.0;
  double t_end   = 3.0;

  // Add event listeners
  SpeedUp longclick=SpeedUp(mesh, mesh2);		// Speed up on left long click
  viewer.add_listener("LongClick",&longclick);	
  SlowDown rightlongclick=SlowDown(mesh, mesh2);	// Speed up on right right click
  viewer.add_listener("RightLongClick",&rightlongclick);	
  PauseResume keydown=PauseResume(dt);			// Pause on 'p' resume on 'r'
  viewer.add_listener("KeyDown",&keydown);

  // Start the spheres' movement
  for (double t = t_start; t < t_end; t += dt) {
	    //Setting Forces
	    DashpotForce dashpot(10000,0.001); // 1000 0.001
	    VolumePenaltyForce volumePenalty(&mesh, 10);

	    //Final Force
	    auto f = make_combined_force( dashpot, volumePenalty);

	    //Setting Box Constraints 
   	    CubeConstraint constraint(-2);

	    // check for collision
	    c.check_collisions();
	    size_t count = std::distance(c.begin(), c.end());
	    if (count>0) {
		bounce_apart(mesh, mesh2);
	    }

	    //Final Version - Add constraints
	    symp_euler_step(mesh, t, dt, f, constraint);
	    symp_euler_step(mesh2, t, dt, f, constraint);

	    // Update viewer with nodes' new positions  
        viewer.add_nodes(mesh.node_begin(), mesh.node_end(), node_map);  
        viewer.add_nodes(mesh2.node_begin(), mesh2.node_end(), node_map2);
  

        viewer.set_label(t);

  }
  return 0;
}
