/**
 * @file poisson.cpp
 * Test script for treating the Graph as a MTL Matrix
 * and solving a Poisson equation.
 *
 * @brief Reads in two files specified on the command line.
 * First file: 3D Points (one per line) defined by three doubles.
 * Second file: Eges (one per line) defined by 2 indices into the point list
 *              of the first file.
 *
 * Launches an SDLViewer to visualize the solution.
 */

#include "CS207/SDLViewer.hpp"
#include "CS207/Util.hpp"

#include "Graph.hpp"
#include "Point.hpp"
#include "BoundingBox.hpp"

#include <boost/numeric/mtl/mtl.hpp>
#include <boost/numeric/itl/itl.hpp>

using namespace std;

typedef Graph<bool,char> GraphType;
typedef GraphType::Node Node;

typedef mtl::dense_vector<double> VectorType;

struct  GraphSymmetricMatrix
{
    /** Constructor */
  GraphSymmetricMatrix(GraphType& graph):graph_(&graph){
    tagNodes();
    n = graph_->size();
  }

  /** Helper function to perform multiplication . Allows for delayed
  * evaluation of results .
  * Assign :: apply (a, b) resolves to an assignment operation such as
  * a += b, a -= b, or a = b.
  * @pre @a size (v) == size (w) */
  template <typename VectorIn , typename VectorOut , typename Assign >
  void mult ( const VectorIn & v, VectorOut & w, Assign ) const{
    //make sure multiplication is valid
    assert(size(v) == size(w));

    for (int i = 0; i < n; i++){
      double total = 0;
      for (int j = 0; j< n; j++){
        total += Laplace(i, j) * v[j];  
      }
      w[i] = Assign::apply(w[i], v[i]);
    }

  }

  /** Matrix - vector multiplication forwards to MTL ’s lazy mat_cvec_multiplier operator */
  template <typename Vector>
  mtl::vec::mat_cvec_multiplier<GraphSymmetricMatrix, Vector>
  operator *(const Vector& v) const {
    return mtl::vec::mat_cvec_multiplier<GraphSymmetricMatrix, Vector>(* this , v);
  }

  double Laplace(size_t i, size_t j) const{
    bool equal = i==j;
    bool iboundryNode = graph_->node(i).value();
    bool jboundryNode = graph_->node(j).value();

    if(equal && iboundryNode) return 1;
    else if (!equal && (iboundryNode || jboundryNode)) return 0;
    else {
      if (equal) return -1.0 * graph_->node(i).degree();
      else if (graph_->has_edge(graph_->node(i), graph_->node(j))) return 1;
      else return 0;
    }

  }

  // number of rowns and columns in square matrix
  size_t n;

private:
  GraphType* graph_;
  
  // use this to tag boundry nodes
  void tagNodes () {
    const BoundingBox box(Point(-0.6, -0.2, -1.0), Point(0.6, 0.2, 1.0));
    for (auto itr = graph_->node_begin(); itr != graph_->node_end(); ++itr){
      Point p = (*itr).position();
      bool norm1 = norm_inf(p) == 1;
      bool secondConditon = norm_inf(p - Point(0.6, 0.6, 0.0)) < 0.2 || norm_inf(p - Point(-0.6, 0.6, 0.0)) < 0.2 
                          || norm_inf(p - Point(0.6, -0.6, 0.0)) < -0.2 || norm_inf(p - Point(-0.6, -0.6, 0.0)) < 0.2;
      bool inBox = box.contains(p);
      if (norm1 || secondConditon || inBox){
        (*itr).value() = true;
      } 
        
    }
  }

};

/** The number of elements in the matrix . */
inline std :: size_t size ( const GraphSymmetricMatrix & A) {
  return A.n * A.n;
}

/** The number of rows in the matrix . */
inline std :: size_t num_rows ( const GraphSymmetricMatrix & A) {
  return A.n;
}

/** The number of columns in the matrix . */
inline std :: size_t num_cols ( const GraphSymmetricMatrix & A) {
  A.n;
}

/* Functor to update the position*/
template<typename Vector>
struct UpdatePosition
{
  Vector u_;

  UpdatePosition(const Vector& u): u_(u){};

  Point operator() (const Node& a) const {
    //std::cout << u_[a.index()] << std::endl;
    return Point(a.position().x, a.position().y, u_[a.index()]);
  }
};

// calls the color map
struct ColorType {
  CS207::Color operator()(const Graph<bool, char>::node_type& n) {
    return CS207::Color::make_heat(abs(n.position().y));
  }
};

/** Traits that MTL uses to determine properties of our IdentityMatrix . */
namespace mtl {
  namespace ashape {
    /** Define IdentityMatrix to be a non - scalar type . */
    template < >
    struct ashape_aux < GraphSymmetricMatrix > {
      typedef nonscal type ;
    };
  } // end namespace ashape

  /** IdentityMatrix implements the Collection concept
  * with value_type and size_type */
  template < >
  struct Collection < GraphSymmetricMatrix > {
    typedef double value_type ;
    typedef unsigned size_type ;
  };
} // end namespace mtl

namespace itl {
  template <class Real, class ColorTypee, class PositionType, class OStream = std::ostream>
  class visual_iteration : public cyclic_iteration<Real> 
  {
    typedef cyclic_iteration<Real> super;
            
    public:
    template <class Vector>
    visual_iteration(const Vector& r0, int max_iter_, Real tol_, Real atol_, int cycle_, GraphType& graph,
      CS207::SDLViewer& viewer, VectorType& newVector, ColorTypee colorFunction, PositionType positionFunction, OStream& out = std::cout)
    :super(r0, max_iter_, tol_, atol_, cycle_),
      viewer_(viewer), graph_(graph), newVector_(newVector), colorFunction_(colorFunction), positionFunction_(positionFunction)
    {
      initialize();
    }

    bool finished() { updateViewer(); return super::finished(); } 
    template <typename T>
    bool finished(const T& r) 
    {
      updateViewer();
      bool ret= super::finished(r);
      return ret;
    }
 
  private:
    GraphType& graph_;
    CS207::SDLViewer& viewer_;
    VectorType& newVector_;
    ColorTypee colorFunction_;
    PositionType positionFunction_;
    std::map<typename GraphType::node_type, unsigned> nodeMap_;

  void updateViewer() {
    viewer_.add_nodes(graph_.node_begin(), graph_.node_end(), colorFunction_, UpdatePosition<VectorType>(newVector_), nodeMap_);
    viewer_.add_edges(graph_.edge_begin(), graph_.edge_end(), nodeMap_);
    viewer_.center_view();
  }

  void initialize(){
    nodeMap_ = viewer_.empty_node_map(graph_);
    viewer_.launch();
    updateViewer();
  }
  };
}

/*g(x) for Laplace*/
double g(const Point& p){
  const BoundingBox box(Point(-0.6, -0.2, -1.0), Point(0.6, 0.2, 1.0));
  bool norm1 = norm_inf(p) == 1;
  bool secondConditon = norm_inf(p - Point(0.6, 0.6, 0.0)) < 0.2 || norm_inf(p - Point(-0.6, 0.6, 0.0)) < 0.2 
                          || norm_inf(p - Point(0.6, -0.6, 0.0)) < -0.2 || norm_inf(p - Point(-0.6, -0.6, 0.0)) < 0.2;
  bool inBox = box.contains(p);
  if (norm1) return 0;
  else if (secondConditon) return -0.2;
  else if (inBox) return 1;
  else return -1;
}

/*f(x) for Laplace*/
double f(const Point& p) {
  return 5*cos(norm_1(p));
}


/** Remove all the nodes in graph @a g whose posiiton is contained within
 * BoundingBox @a bb
 * @post For all i, 0 <= i < @a g.num_nodes(),
 *        not bb.contains(g.node(i).position())
 */
void remove_box(GraphType& g, const BoundingBox& bb) {
  for (auto itr = g.node_begin(); itr != g.node_end();)
  {
    if (bb.contains((*itr).position())) g.remove_node(*itr);
    else ++itr;
  }
}


int main(int argc, char** argv)
{
  // Check arguments
  if (argc < 2) {
    std::cerr << "Usage: " << argv[0] << " NODES_FILE EDGES_FILE\n";
    exit(1);
  }

  // Define an empty Graph
  GraphType graph;

  // Create a nodes_file from the first input argument
  std::ifstream nodes_file(argv[1]);
  // Interpret each line of the nodes_file as a 3D Point and add to the Graph
  std::vector<typename GraphType::node_type> node_vec;
  Point p;
  while (CS207::getline_parsed(nodes_file, p))
    node_vec.push_back(graph.add_node(2*p - Point(1,1,0)));

  // Create a tets_file from the second input argument
  std::ifstream tets_file(argv[2]);
  // Interpret each line of the tets_file as four ints which refer to nodes
  std::array<int,4> t;
  while (CS207::getline_parsed(tets_file, t)) {
    graph.add_edge(node_vec[t[0]], node_vec[t[1]]);
    graph.add_edge(node_vec[t[0]], node_vec[t[2]]);
    graph.add_edge(node_vec[t[1]], node_vec[t[3]]);
    graph.add_edge(node_vec[t[2]], node_vec[t[3]]);
  }

  // Get the edge length, should be the same for each edge
  double h = graph.edge(0).length();

  // Make holes in our Graph
  remove_box(graph, BoundingBox(Point(-0.8+h,-0.8+h,-1), Point(-0.4-h,-0.4-h,1)));
  remove_box(graph, BoundingBox(Point( 0.4+h,-0.8+h,-1), Point( 0.8-h,-0.4-h,1)));
  remove_box(graph, BoundingBox(Point(-0.8+h, 0.4+h,-1), Point(-0.4-h, 0.8-h,1)));
  remove_box(graph, BoundingBox(Point( 0.4+h, 0.4+h,-1), Point( 0.8-h, 0.8-h,1)));
  remove_box(graph, BoundingBox(Point(-0.6+h,-0.2+h,-1), Point( 0.6-h, 0.2-h,1)));

  // Define b using the graph, f, and g.
  // Construct the GraphSymmetricMatrix A using the graph
  // Solve Au = b using MTL.

  // initialize matrix
  typedef GraphSymmetricMatrix matrix_type;
  matrix_type A(graph);

  //get size
  int num = graph.size();

  //initalize b and u in Au = b
  VectorType u(num, 0.0);
  VectorType b(num, 0.0);

  for (int i = 0; i < num; i++){
    // if not a boundry node
    if (!(graph.node(i).value())){
      b[i] = h * h * f(graph.node(i).position());
      
      double sum = 0;
      for (auto itr = graph.node(i).edge_begin(); itr != graph.node(i).edge_end(); ++itr){
        if ((*itr).node2().value()){
          sum += g((*itr).node2().position());
        }
      }
      b[i] -= sum;
    } else {
      b[i] = g(graph.node(i).position());
    }
  }

  itl::pc::identity<matrix_type>P(A);
  UpdatePosition<VectorType>updatePos(u);
  CS207::SDLViewer viewer;
  itl::visual_iteration<double, ColorType, UpdatePosition<VectorType>>
    vis_iter(b, 500, 1.e-10, 0.0, 25, graph, viewer, u, ColorType(), updatePos);
  cg(A, u, b, P, vis_iter);
  return 0;
}
